= System Documentation for Developers =

I. Fundamentals:
 A. Qubes OS Architecture v0.3 [http://files.qubes-os.org/files/doc/arch-spec-0.3.pdf (pdf)] (The original 2009 document that started this all...)
 A. [wiki:SecurityCriticalCode Security-critical elements of Qubes OS ]
 A. Qubes RPC framework (qrexec): 
   1. [wiki:Qrexec The Qubes RPC/Service API]
   1. Example for writing a qrexec service in Qubes OS [http://theinvisiblethings.blogspot.com/2013/02/converting-untrusted-pdfs-into-trusted.html blog post] 
   1. [wiki:Qrexec2Implementation qrexec implementation in Qubes R2]
   1. [wiki:Qrexec3Implementation qrexec implementation in Qubes R3/Odyssey]
 A. [wiki:GUIdocs Qubes GUI virtualization protocol ]
 A. [wiki:QubesNet Networking in Qubes ]
 A. [wiki:TemplateImplementation Implementation of template sharing and updating]
I. Services:
 A. [wiki:Qfilecopy Inter-domain file copying ]
 A. [wiki:Qmemman Dynamic memory management in Qubes ]
 A. [wiki:DVMimpl Implementation of DisposableVMs ]
 A. [http://theinvisiblethings.blogspot.com/2010/06/disposable-vms.html Article about disposable VMs]
 A. [wiki:Dom0SecureUpdates Dom0 secure update mechanism]
 A. [wiki:VMSecureUpdates VM secure update mechanism]
I. Debugging:
 A. [wiki:Profiling Profiling python code]
 A. [wiki:TestBench Test environment in separate machine for automatic tests]
 A. [wiki:SystemDoc/VMInterface VM-dom0 internal configuration interface]
 A. [wiki:WindowsDebugging Debugging Windows VMs]
I. Building:
 A. [wiki:QubesBuilder Building Qubes]
 A. [[wiki:DevelopmentWorkflow|Development Workflow]]
 A. [wiki:KdeDom0 KDE Dom0 packages for Qubes]
 A. [wiki:InstallationIsoBuilding How to build Qubes installation ISO]
 A. [wiki:USBVM Building USB passthrough support (experimental)]
 A. [wiki:BuildingNonFedoraTemplate Building a TemplateVM based on a new OS (ArchLinux example)]
 A. [wiki:BuildingArchlinuxTemplate Building the Archlinux Template]