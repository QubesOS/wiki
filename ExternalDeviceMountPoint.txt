All external storage devices connected to an AppVM using the Fedora template can be found under

{{{
/run/media/user/
}}}

...of that AppVM's filesystem.