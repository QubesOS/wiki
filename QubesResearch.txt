Here are some links to various papers/research projects that somehow relate to Qubes.

=== Attacks on Intel TXT ===
 * [http://invisiblethingslab.com/resources/bh09dc/Attacking%20Intel%20TXT%20-%20paper.pdf "Attacking Intel® Trusted Execution Technology"] by Rafal Wojtczuk, Joanna Rutkowska
 * [http://www.ssi.gouv.fr/IMG/pdf/article_acpi.pdf "ACPI: Design Principles and Concerns"] by Loic Duflot, Olivier Levillain, and Benjamin Morin
 * [http://invisiblethingslab.com/resources/misc09/Another%20TXT%20Attack.pdf "Another Way to Circumvent Intel® Trusted Execution Technology"] by Rafal Wojtczuk, Joanna Rutkowska, Alex Tereshkin
 * [http://www.invisiblethingslab.com/resources/2011/Attacking_Intel_TXT_via_SINIT_hijacking.pdf "Attacking Intel TXT® via SINIT code execution hijacking"] by Rafal Wojtczuk and Joanna Rutkowska

=== Software attacks coming through devices ===
 * [http://www.ssi.gouv.fr/IMG/pdf/csw-trustnetworkcard.pdf "Can you still trust your network card?"] by Loïc Duflot, Yves-Alexis Perez and others
 * [http://theinvisiblethings.blogspot.com/2010/04/remotely-attacking-network-cards-or-why.html "Remotely Attacking Network Cards (or why we do need VT-d and TXT)"] by Joanna Rutkowska
 * [http://theinvisiblethings.blogspot.com/2010/05/on-formally-verified-microkernels-and.html "On Formally Verified Microkernels (and on attacking them)"] by Joanna Rutkowska
 * [http://www.invisiblethingslab.com/resources/2011/Software%20Attacks%20on%20Intel%20VT-d.pdf "Following the White Rabbit: Software Attacks against Intel® VT-d"] by Rafal Wojtczuk and Joanna Rutkowska

=== Application-level security ===
 * [http://radlab.cs.berkeley.edu/wiki/Virtics "Virtics: A System for Privilege Separation of Legacy Desktop Applications"] by Matt Piotrowski
 [[BR]](We plan to implement some ideas from Matt's thesis in Qubes very soon -- stay tuned for details)

=== VMM/Xen disagregation
 * [http://tjd.phlegethon.org/words/sosp11-xoar.pdf "Breaking Up is Hard to Do: Security and Functionality in a
 Commodity Hypervisor] by Patrick Colp at el.
 [[BR]] (Also see [http://www.gossamer-threads.com/lists/xen/devel/230011 this thread on xen-devel]) 